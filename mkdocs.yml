site_name: sts - Storage Tests
site_description: Linux storage testing framework using pytest and
  testinfra
site_url: https://rh-kernel-stqe.gitlab.io/sts
repo_url: https://gitlab.com/rh-kernel-stqe/sts
repo_name: rh-kernel-stqe/sts

theme:
  name: material
  palette:
    scheme: slate
    primary: indigo
    accent: indigo
  features:
    - navigation.instant
    - navigation.tracking
    - navigation.sections
    - navigation.expand
    - toc.follow
    - content.code.copy

markdown_extensions:
  - admonition
  - pymdownx.details
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.highlight
  - pymdownx.inlinehilite
  - tables
  - toc:
      permalink: true

plugins:
  - search
  - mkdocstrings:
      handlers:
        python:
          paths: [sts_libs/src]
          options:
            docstring_style: google
            show_source: true
            show_root_heading: true
            heading_level: 2

nav:
  - Home: index.md
  - sts-libs:
      - Overview: sts-libs/index.md
      - Test Fixtures: sts-libs/fixtures.md
      - Storage Libraries:
          - Base Classes: sts-libs/libraries/base.md
          - Block Device: sts-libs/libraries/blockdevice.md
          - Device Mapper: sts-libs/libraries/devicemapper.md
          - Fibre Channel: sts-libs/libraries/fc.md
          - iSCSI: sts-libs/libraries/iscsi.md
          - Loop Device: sts-libs/libraries/loop.md
          - LVM: sts-libs/libraries/lvm.md
          - Multipath: sts-libs/libraries/multipath.md
          - NVMe: sts-libs/libraries/nvme.md
          - SCSI: sts-libs/libraries/scsi.md
          - SCSI Debug: sts-libs/libraries/scsi_debug.md
          - SCSI Target: sts-libs/libraries/target.md
          - Stratis: sts-libs/libraries/stratis.md
          - VDO: sts-libs/libraries/vdo.md
      - Utilities: sts-libs/utils.md
  - Contributing:
      - Overview: contributing.md
      - Docstring Style: contributing/docstrings.md
