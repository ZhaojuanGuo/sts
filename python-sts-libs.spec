Name:           python-sts-libs
Version:        0.0.0
Release:        %autorelease
Summary:        Library for pytest-based Linux storage tests


License:        GPL-3.0-or-later
URL:            https://pypi.org/project/sts-libs/
Source:         %{pypi_source sts_libs}

BuildArch:      noarch
BuildRequires:  python3-devel

%global _description %{expand:
Library for storage testing with pytest.
Used by sts tests: https://gitlab.com/rh-kernel-stqe/sts
}

%description %_description

%package -n     python3-sts-libs
Summary:        %{summary}

%description -n python3-sts-libs %_description

%prep
%autosetup -p1 -n sts_libs-%{version}

%generate_buildrequires
%pyproject_buildrequires

%build
%pyproject_wheel

%install
%pyproject_install
%pyproject_save_files sts

%check
%pyproject_check_import

%files -n python3-sts-libs -f %{pyproject_files}

%changelog
%autochangelog
