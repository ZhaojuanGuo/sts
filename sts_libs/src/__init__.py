"""Storage Testing System (STS) library.

This package provides functionality for testing storage systems:
- Device management
- Network configuration
- Test fixtures
- Utility functions
"""
