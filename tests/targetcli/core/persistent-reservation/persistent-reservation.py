#  Copyright: Contributors to the sts project
#  GNU General Public License v3.0+ (see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt)

"""Tests using sg_persist to reserve iSCSI LUN."""

from os import getenv

import pytest

from sts import iscsi, lio, scsi
from sts.utils.cmdline import run

t_iqn = getenv('TARGET_IQN', default='iqn.2017-11.com.redhat:pr-target')
i_iqn = getenv('INITIATOR_IQN', default='iqn.2017-11.com.redhat:pr-initiator')


@pytest.mark.usefixtures('_iscsi_localhost_test')
def test_persistent_reservation() -> None:
    """ISCSI lun persistent reservation test."""
    iscsi.set_initiatorname(i_iqn)
    # Target setup
    assert lio.create_basic_iscsi_target(
        target_wwn=t_iqn,
        initiator_wwn=i_iqn,
        size='128M',
    )
    # Connect to the target locally
    iscsi.discovery_st('127.0.0.1', disc_db=True, ifaces='default')
    assert iscsi.node_login(portal='127.0.0.1', udev_wait_time=5)
    # Get the iscsi lun
    backstore_name = i_iqn.split(':')[1]
    dev = scsi.get_free_disks(filter_only={'model': backstore_name})
    if not dev:
        pytest.fail('Could not find device to use...')
    test_dev = '/dev/' + next(iter(dev.keys()))
    # To register a key on a LUN
    assert run(f'sg_persist --out --register --param-sark=0xDEADBEEF {test_dev}').succeeded
    # To view the keys registered on a LUN
    assert run(f'sg_persist --in -k -d {test_dev}').succeeded
    # Take out a reservation on behalf of a key
    # The reservation types are defined in the SCSI Primary Commands spec.
    # Here are the reservation types from the sg_persist manpage
    # 1-> write exclusive
    # 3-> exclusive access
    # 5-> write exclusive - registrants only
    # 6-> exclusive access - registrants only
    # 7-> write exclusive - all registrants
    # 8-> exclusive access - all registrants.
    assert run(f'sg_persist --out --reserve --param-rk=0xDEADBEEF --prout-type=1 {test_dev}').succeeded
    # To view the reservations currently out on a device
    assert run(f'sg_persist --in -r -d {test_dev}').succeeded
    # Release a reservation
    assert run(f'sg_persist --out --release --param-rk=0xDEADBEEF  --prout-type=1 {test_dev}').succeeded
    # Unregister a key
    assert run(f'sg_persist --out --register --param-rk=0xDEADBEEF {test_dev}').succeeded
