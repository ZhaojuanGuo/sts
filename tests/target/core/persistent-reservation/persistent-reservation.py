#  Copyright: Contributors to the sts project
#  GNU General Public License v3.0+ (see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt)

"""Tests of using sg_persist to reserve iSCSI LUN."""

from typing import Callable

import pytest

from sts.iscsi.config import IscsiNode
from sts.utils.cmdline import run


@pytest.mark.parametrize('iscsi_target', [{'size': '2G', 'n_luns': 1}], indirect=True)
def test_persistent_reservation(iscsi_target: IscsiNode, get_test_device: Callable) -> None:  # noqa: ARG001
    """
    Verifies the ability to manage persistent reservations on an iSCSI LUN,
    including registering keys, taking out reservations, and releasing reservations.

    The reservation types are defined in the SCSI Primary Commands spec.
    Here are the reservation types from the sg_persist manpage.
    1-> write exclusive
    3-> exclusive access
    5-> write exclusive - registrants only
    6-> exclusive access - registrants only
    7-> write exclusive - all registrants
    8-> exclusive access - all registrants.

    Args:
        iscsi_target (IscsiNode): The iSCSI target node for testing.
        get_test_device (Callable): A callable that returns the test device path.
    """
    iscsi_lun = get_test_device()

    # To register a key on a LUN
    assert run(f'sg_persist --out --register --param-sark=0xDEADBEEF {iscsi_lun}').succeeded

    # To view the keys registered on a LUN
    assert run(f'sg_persist --in -k -d {iscsi_lun}').succeeded

    # Take out a reservation on behalf of a key
    assert run(f'sg_persist --out --reserve --param-rk=0xDEADBEEF --prout-type=1 {iscsi_lun}').succeeded

    # To view the reservations currently out on a device
    assert run(f'sg_persist --in -r -d {iscsi_lun}').succeeded

    # Release a reservation
    assert run(f'sg_persist --out --release --param-rk=0xDEADBEEF  --prout-type=1 {iscsi_lun}').succeeded

    # Unregister a key
    assert run(f'sg_persist --out --register --param-rk=0xDEADBEEF {iscsi_lun}').succeeded
