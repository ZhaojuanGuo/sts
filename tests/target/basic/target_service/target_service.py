"""Test target.service functionality."""

import time

from sts.utils.system import SystemManager


def test_target_service() -> None:
    """Test target.service operations.

    Tests that target.service can be:
    1. Enabled/disabled
    2. Started/stopped
    3. Restarted
    """
    system = SystemManager()
    service = 'target.service'

    # Save initial state
    was_enabled = system.is_service_enabled(service)
    was_running = system.is_service_running(service)

    try:
        # Test enable/disable cycle
        if was_enabled:
            assert system.service_disable(service)
            assert system.service_enable(service)
        else:
            assert system.service_enable(service)
            assert system.service_disable(service)

        # Test start/stop/restart cycle
        if was_running:
            assert system.service_stop(service)
            time.sleep(1)  # Wait for service to settle
            assert system.service_start(service)
            time.sleep(2)
            assert system.service_restart(service)
        else:
            assert system.service_start(service)
            time.sleep(2)  # Wait for service to settle
            assert system.service_restart(service)
            time.sleep(2)
            assert system.service_stop(service)

    finally:
        # Restore initial state
        if was_enabled:
            system.service_enable(service)
        else:
            system.service_disable(service)

        if was_running:
            system.service_start(service)
        else:
            system.service_stop(service)
