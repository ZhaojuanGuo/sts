"""Test iSCSI target functionality."""

import pytest

from sts.iscsi.iscsiadm import IscsiAdm
from sts.utils.cmdline import run

arguments_list = [
    {
        't_iqn': 'iqn.2003-01.com.redhat:targettest1',
        'i_iqn': 'iqn.2003-01.com.redhat:initiatortest1',
        'n_luns': 256,
        'back_size': '1M',
    },
    {
        't_iqn': 'iqn.2003-01.com.redhat:targettest2',
        'i_iqn': 'iqn.2003-01.com.redhat:initiatortest2',
        'n_luns': 1,
        'back_size': '1G',
    },
    {
        't_iqn': 'iqn.2003-01.com.redhat:targettest3',
        'i_iqn': 'iqn.2003-01.com.redhat:initiatortest3',
        'n_luns': 0,
        'back_size': '1G',
    },
]


@pytest.mark.parametrize('iscsi_target_setup', arguments_list, indirect=True)
@pytest.mark.usefixtures('iscsi_target_setup')
def test_iscsi_target() -> None:
    """Test iSCSI target functionality.

    This test:
    1. Creates targets with different configurations
    2. Verifies discovery works
    3. Tests login/logout
    """
    iscsiadm = IscsiAdm()

    assert iscsiadm.discovery(portal='127.0.0.1:3260').succeeded
    assert iscsiadm.node_login()
    assert iscsiadm.node_logoutall()

    run('rm -rf ./backstore[[:digit:]]*_file')
