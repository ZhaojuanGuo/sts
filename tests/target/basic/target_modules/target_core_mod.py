"""Test target_core_mod module loading."""

from contextlib import suppress
from time import sleep

from sts.utils.errors import ModuleInUseError
from sts.utils.modules import ModuleInfo, ModuleManager


def test_target_core_mod() -> None:
    """Test loading and unloading target_core_mod.

    The module has several dependencies that need to be unloaded first:
    - target_core_pscsi
    - target_core_file
    - target_core_iblock
    - iscsi_target_mod
    """
    mm = ModuleManager()
    module_name = 'target_core_mod'

    # Load/unload module 20 times
    for _ in range(20):
        assert mm.load(module_name)
        sleep(5)
        # Need to unload with dependencies since other modules depend on it
        with suppress(ModuleInUseError):  # Pass the test if failing to unload because of 'module is in use'
            assert mm.unload_with_dependencies(module_name)
        sleep(5)

    # Final load and verify
    assert mm.load(module_name)
    info = ModuleInfo.from_name(module_name)
    assert info
