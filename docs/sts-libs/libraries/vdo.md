# VDO

This section documents the Virtual Data Optimizer (VDO) functionality,
which provides block-level deduplication and compression.

::: sts.vdo
