# Stratis

This section documents the Stratis storage management functionality.

## Base Functionality

::: sts.stratis.base

## Pool Management

::: sts.stratis.pool

## Filesystem Management

::: sts.stratis.filesystem
