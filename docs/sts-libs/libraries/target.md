# Target

This section documents the target functionality, which provides interfaces
for working with SCSI target subsystem configuration and management.

::: sts.target
