# Block Device

This section documents the base block device functionality, which provides common
operations and properties for working with block devices.

::: sts.blockdevice
